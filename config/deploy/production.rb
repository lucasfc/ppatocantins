set :port, 22
set :user, 'ppa'
set :deploy_via, :remote_cache
#set :use_sudo, false

server '192.168.10.101',
  roles: [:web, :app, :db],
  port: fetch(:port),
  user: fetch(:user),
  primary: true

set :deploy_to, "/home/#{fetch(:user)}/apps/#{fetch(:application)}"

set :ssh_options, {
  forward_agent: true,
  auth_methods: %w(publickey),
  user: 'ppa',
}

set :rails_env, :production
set :conditionally_migrate, true    

#ngix
#set :nginx_domains, "alerta.euprefeito.com"

# Roles the deploy nginx site on,
# default value: :web
#set :nginx_roles, :web

# Socket file that nginx will use as upstream to serve the application
# Note: Socket upstream has priority over host:port upstreams
# no default value
#set :app_server_socket, "/tmp/unicorn.participativo.sock"