root = "/home/sistemas/apps/ppa2/current"
working_directory root
pid "#{root}/tmp/pids/unicorn.ppa2.pid"
stderr_path "#{root}/log/unicorn_err.log"
stdout_path "#{root}/log/unicorn_out.log"

listen "/tmp/unicorn.ppa2.sock"
worker_processes 2
timeout 30

# Force the bundler gemfile environment variable to
# reference the capistrano "current" symlink
before_exec do |_|
  ENV["BUNDLE_GEMFILE"] = File.join(root, 'Gemfile')
end