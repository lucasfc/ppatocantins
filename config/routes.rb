Rails.application.routes.draw do
  #get 'site' => "site#index", as: :site_index

  get 'web' => "web#index", as: :web_index
  get 'web/document/:id' => "web#document", as: :web_document
  get 'web/presentation' , as: :web_presentation
  get 'web/document'
  get 'web/certificado'
  get 'cadastro/cadastrar', as: :cadastrar
  post 'cadastro/cadastrar' => "cadastro#create"

  resources :reply_deeds do
    member do 
      get "delete_eq"
    end
  end

  resources :deeds

  devise_for :users
  resources :configurations

  resources :questions

  resources :comments do
    member do
      post 'editable_edit'
    end
  end

  resources :votes do
    collection do
      post 'create_by_reply'
      get 'receber_voto'
      get 'entrada_sala'
    end
    member do
      get  'plus_vote'
    end
  end

  resources :replies

  resources :documents

  resources :users do
    collection do
      get 'tokens'
    end
  end

  get 'home/index'
  get 'home/credenciamento', as: :credenciamento
  get 'home/painel_credenciamento', as: :painel_credenciamento
  get 'home/painel_cidades', as: :painel_cidades
  get 'home/painel_categorias', as: :painel_categorias
  get 'home/painel_moderador',  as: :painel_moderador
  get 'home/painel_redator', as: :painel_redator
  get 'home/lista_participantes', as: :lista_participantes
  get 'home/plenaria', as: :plenaria
  get 'home/relatorio_evento', as: :relatorio_evento
  get 'home/get_actions', as: :get_actions
  get 'home/painel'
  get 'home/telao', as: :telao
  get 'home/multimidia', as: :multimidia
  get 'home/multimidia_control', as: :multimidia_control
  get 'home/painel_votacao_final', as: :painel_votacao_final
  
  resources :themes do
   

  end

  resources :entries do
    member do
      get "etiqueta"
      get "print"
      get "certificado"
    end
  end

  resources :people

  resources :events do
    member do 
      get 'names'
      get 'votacao_final'
      get 'definir_prioritario'
      get 'retornar_desafios'
      get 'retornar_perguntas_plenarias'
      get 'finalizar_votacao'
      
    end
    collection do
      get "contabilizar_votos"
      get 'contabilizar_votos_plenaria'
    end

  end


  get "credenciar/" => "home#credenciar", as: :credenciar_sem
  get "credenciar/:cpf" => "home#credenciar", as: :credenciar
  get "painel/norteadoras/:question_id/:theme_id" => "home#get_norteadoras", as: :get_norteadoras
  get "painel/sociedade/:question_id/:theme_id" => "home#get_sociedade", as: :get_sociedade

  root 'home#index'


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
