lock '3.3.5'

set :application, 'ppa'
set :pty, true
set :repo_url, 'http://lucasfc@bitbucket.org/lucasfc/ppatocantins.git'#'git@git.seplan.to.gov.br:lucas/ppa.git'
set :branch, "master"


#ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call


set :bundle_binstubs, nil
set :linked_files, fetch(:linked_files, []).push('config/database.yml')
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

after 'deploy:publishing', 'deploy:restart'
set :use_sudo, false

namespace :deploy do
  task :restart do
    invoke 'unicorn:legacy_restart'
  end
end