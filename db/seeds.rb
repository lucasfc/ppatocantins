# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

City.delete_all
City.create(
	JSON.load(open("#{Rails.root}/db/seeds/cidades.json"))
)

Theme.delete_all
Theme.create(
	JSON.load(open("#{Rails.root}/db/seeds/themes.json"))
)

Subtheme.delete_all
Subtheme.create(
	JSON.load(open("#{Rails.root}/db/seeds/subthemes.json"))
)

Deed.delete_all
Deed.create(
	JSON.load(open("#{Rails.root}/db/seeds/deeds.json"))
)



User.create(
	name: "ADMIN",
	email: "admin@admin.com",
	password: "sepl@n2015",
	password_confirmation: "sepl@n2015",
	admin: true
)