class RemoveNameFromDeed < ActiveRecord::Migration
  def change
    remove_column :deeds, :name, :string
  end
end
