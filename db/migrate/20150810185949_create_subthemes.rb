class CreateSubthemes < ActiveRecord::Migration
  def change
    create_table :subthemes do |t|
      t.string :name
      t.references :theme, index: true
      t.text :description

      t.timestamps
    end
  end
end
