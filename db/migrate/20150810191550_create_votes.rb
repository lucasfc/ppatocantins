class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.references :reply, index: true
      t.references :subtheme, index: true
      t.references :event, index: true
      t.integer :value

      t.timestamps
    end
  end
end
