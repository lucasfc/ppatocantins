class AddVoteToReplyDeeds < ActiveRecord::Migration
  def change
    add_reference :reply_deeds, :vote, index: true
  end
end
