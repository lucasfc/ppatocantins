class AddThemeToVote < ActiveRecord::Migration
  def change
    add_reference :votes, :theme, index: true
  end
end
