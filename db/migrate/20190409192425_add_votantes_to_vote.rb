class AddVotantesToVote < ActiveRecord::Migration
  def change
    add_reference :votes, :person, index: true
  end
end
