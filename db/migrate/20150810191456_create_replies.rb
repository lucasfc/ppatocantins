class CreateReplies < ActiveRecord::Migration
  def change
    create_table :replies do |t|
      t.string :title
      t.text :description
      t.references :question, index: true
      t.boolean :default
      t.references :subtheme, index: true

      t.timestamps
    end
  end
end
