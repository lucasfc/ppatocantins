class AddLunchToEntrie < ActiveRecord::Migration
  def change
    add_column :entries, :lunch, :boolean
  end
end
