class CreateReplyDeeds < ActiveRecord::Migration
  def change
    create_table :reply_deeds do |t|
      t.references :reply, index: true
      t.references :deed, index: true

      t.timestamps
    end
  end
end
