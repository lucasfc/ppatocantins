class CreateEntries < ActiveRecord::Migration
  def change
    create_table :entries do |t|
      t.references :event, index: true
      t.references :person, index: true
      t.references :theme, index: true

      t.timestamps
    end
  end
end
