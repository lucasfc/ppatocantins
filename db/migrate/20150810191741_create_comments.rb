class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.references :question, index: true
      t.references :subtheme, index: true
      t.references :event, index: true
      t.text :text
      t.string :type

      t.timestamps
    end
  end
end
