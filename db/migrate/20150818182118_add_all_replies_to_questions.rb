class AddAllRepliesToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :all_replies, :boolean
  end
end
