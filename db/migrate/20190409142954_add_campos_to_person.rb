class AddCamposToPerson < ActiveRecord::Migration
  def change
    add_column :people, :sex, :integer
    add_column :people, :company, :string
  end
end
