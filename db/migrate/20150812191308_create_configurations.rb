class CreateConfigurations < ActiveRecord::Migration
  def change
    create_table :configurations do |t|
      t.string :token
      t.text :text

      t.timestamps
    end
  end
end
