class AddThemeToComments < ActiveRecord::Migration
  def change
    add_reference :comments, :theme, index: true
  end
end
