class AddCamposToEntry < ActiveRecord::Migration
  def change
    add_column :entries, :tags, :string
    add_column :entries, :flag, :boolean
  end
end
