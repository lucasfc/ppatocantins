class CreateEntryVotes < ActiveRecord::Migration
  def change
    create_table :entry_votes do |t|
      t.references :entry, index: true
      t.references :vote, index: true

      t.timestamps
    end
  end
end
