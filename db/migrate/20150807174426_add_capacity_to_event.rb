class AddCapacityToEvent < ActiveRecord::Migration
  def change
    add_column :events, :capacity, :integer
    add_column :events, :capacity_room, :integer
  end
end
