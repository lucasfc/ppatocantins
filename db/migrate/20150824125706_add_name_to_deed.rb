class AddNameToDeed < ActiveRecord::Migration
  def change
    add_column :deeds, :name, :text
  end
end
