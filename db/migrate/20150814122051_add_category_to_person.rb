class AddCategoryToPerson < ActiveRecord::Migration
  def change
    add_column :people, :category, :string
  end
end
