class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :cpf
      t.string :name
      t.string :phone
      t.string :email
      t.string :function

      t.timestamps
    end
  end
end
