class CreateDeeds < ActiveRecord::Migration
  def change
    create_table :deeds do |t|
      t.string :name
      t.string :description
      t.references :subtheme, index: true
      t.string :code

      t.timestamps
    end
  end
end
