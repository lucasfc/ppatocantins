class AddFileToDocument < ActiveRecord::Migration
  def change
    add_column :documents, :file, :text
  end
end
