class AddQuestionthemeToQuestion < ActiveRecord::Migration
  def change
    add_reference :questions, :theme, index: true
  end
end
