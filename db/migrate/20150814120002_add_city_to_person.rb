class AddCityToPerson < ActiveRecord::Migration
  def change
    add_reference :people, :city, index: true
  end
end
