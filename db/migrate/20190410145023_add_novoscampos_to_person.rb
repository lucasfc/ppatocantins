class AddNovoscamposToPerson < ActiveRecord::Migration
  def change
    add_column :people, :birthdate, :date
    add_column :people, :mothername, :string
  end
end
