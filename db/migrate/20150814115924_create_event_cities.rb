class CreateEventCities < ActiveRecord::Migration
  def change
    create_table :event_cities do |t|
      t.references :city, index: true
      t.references :event, index: true

      t.timestamps
    end
  end
end
