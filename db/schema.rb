# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190423190652) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "cities", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "comments", force: true do |t|
    t.integer  "question_id"
    t.integer  "subtheme_id"
    t.integer  "event_id"
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "theme_id"
    t.string   "tipo"
  end

  add_index "comments", ["event_id"], name: "index_comments_on_event_id", using: :btree
  add_index "comments", ["question_id"], name: "index_comments_on_question_id", using: :btree
  add_index "comments", ["subtheme_id"], name: "index_comments_on_subtheme_id", using: :btree
  add_index "comments", ["theme_id"], name: "index_comments_on_theme_id", using: :btree

  create_table "configurations", force: true do |t|
    t.string   "token"
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "deeds", force: true do |t|
    t.string   "description"
    t.integer  "subtheme_id"
    t.string   "code"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "name"
  end

  add_index "deeds", ["subtheme_id"], name: "index_deeds_on_subtheme_id", using: :btree

  create_table "documents", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "theme_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "file"
  end

  add_index "documents", ["theme_id"], name: "index_documents_on_theme_id", using: :btree
  add_index "documents", ["user_id"], name: "index_documents_on_user_id", using: :btree

  create_table "entries", force: true do |t|
    t.integer  "event_id"
    t.integer  "person_id"
    t.integer  "theme_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "lunch"
    t.string   "tags"
    t.boolean  "flag"
  end

  add_index "entries", ["event_id"], name: "index_entries_on_event_id", using: :btree
  add_index "entries", ["person_id"], name: "index_entries_on_person_id", using: :btree
  add_index "entries", ["theme_id"], name: "index_entries_on_theme_id", using: :btree

  create_table "entry_votes", force: true do |t|
    t.integer  "entry_id"
    t.integer  "vote_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "entry_votes", ["entry_id"], name: "index_entry_votes_on_entry_id", using: :btree
  add_index "entry_votes", ["vote_id"], name: "index_entry_votes_on_vote_id", using: :btree

  create_table "event_cities", force: true do |t|
    t.integer  "city_id"
    t.integer  "event_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "event_cities", ["city_id"], name: "index_event_cities_on_city_id", using: :btree
  add_index "event_cities", ["event_id"], name: "index_event_cities_on_event_id", using: :btree

  create_table "events", force: true do |t|
    t.string   "name"
    t.string   "place"
    t.text     "description"
    t.date     "date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "capacity"
    t.integer  "capacity_room"
  end

  create_table "people", force: true do |t|
    t.string   "cpf"
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.string   "function"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "city_id"
    t.string   "category"
    t.integer  "sex"
    t.string   "company"
    t.date     "birthdate"
    t.string   "mothername"
  end

  add_index "people", ["city_id"], name: "index_people_on_city_id", using: :btree

  create_table "questions", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "limit"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "all_replies"
    t.integer  "theme_id"
  end

  add_index "questions", ["theme_id"], name: "index_questions_on_theme_id", using: :btree

  create_table "replies", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "question_id"
    t.boolean  "default"
    t.integer  "subtheme_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "replies", ["question_id"], name: "index_replies_on_question_id", using: :btree
  add_index "replies", ["subtheme_id"], name: "index_replies_on_subtheme_id", using: :btree

  create_table "reply_deeds", force: true do |t|
    t.integer  "reply_id"
    t.integer  "deed_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "vote_id"
  end

  add_index "reply_deeds", ["deed_id"], name: "index_reply_deeds_on_deed_id", using: :btree
  add_index "reply_deeds", ["reply_id"], name: "index_reply_deeds_on_reply_id", using: :btree
  add_index "reply_deeds", ["vote_id"], name: "index_reply_deeds_on_vote_id", using: :btree

  create_table "subthemes", force: true do |t|
    t.string   "name"
    t.integer  "theme_id"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "subthemes", ["theme_id"], name: "index_subthemes_on_theme_id", using: :btree

  create_table "themes", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "icon"
    t.string   "color"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tokens", force: true do |t|
    t.string   "token"
    t.boolean  "used"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tokens", ["user_id"], name: "index_tokens_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "cpf"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.boolean  "admin"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "versions", force: true do |t|
    t.string   "item_type",  null: false
    t.integer  "item_id",    null: false
    t.string   "event",      null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

  create_table "votes", force: true do |t|
    t.integer  "reply_id"
    t.integer  "subtheme_id"
    t.integer  "event_id"
    t.integer  "value"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "theme_id"
    t.integer  "person_id"
    t.boolean  "prioritary"
    t.integer  "votoplenaria"
  end

  add_index "votes", ["event_id"], name: "index_votes_on_event_id", using: :btree
  add_index "votes", ["person_id"], name: "index_votes_on_person_id", using: :btree
  add_index "votes", ["reply_id"], name: "index_votes_on_reply_id", using: :btree
  add_index "votes", ["subtheme_id"], name: "index_votes_on_subtheme_id", using: :btree
  add_index "votes", ["theme_id"], name: "index_votes_on_theme_id", using: :btree

end
