class QuestionsController < ApplicationController
  #before_action :authenticate_user!
  before_action :set_question, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource except: :create
  
  respond_to :html, :json

  #Lista todos os registros
  def index
    @questions = Question.all
    respond_with(@questions)
  end

  #Exibe um registro específico
  def show
    @themes = Theme.all
    respond_with(@question)
  end

  #Exibe o formulário de adição de registro
  def new
    @question = Question.new
    respond_with(@question)
  end

  #Exibe o formulário de edição de registro
  def edit
  end

  #Cria um registro com base em params
  def create
    @question = Question.new(question_params)
    @question.save
    respond_with(@question)
  end

  #Atualiza um registro
  def update
    @question.update(question_params)
    respond_with(@question)
  end

  #Apaga um registro
  def destroy
    @question.destroy
    respond_with(@question)
  end

  private
    def set_question
      @question = Question.find(params[:id])
    end

    def question_params
      params.require(:question).permit(:title, :description, :limit, :all_replies)
    end
end
