class EntriesController < ApplicationController
  #before_action :authenticate_user!, except: [:create]
  load_and_authorize_resource except: :create
  before_action :set_entry, only: [:show, :edit, :update, :destroy, :etiqueta, :print, :certificado]

  # GET /entries
  # GET /entries.json
  def index
    @entries = Entry.all
  end

  # GET /entries/1
  # GET /entries/1.json
  def show
  end

  # GET /entries/new
  def new
    @entry = Entry.new
  end

  # GET /entries/1/edit
  def edit
  end

  # POST /entries
  # POST /entries.json
  def create
    @entry = Entry.new(entry_params)

    respond_to do |format|
      if @entry.save
        format.html { redirect_to @entry, notice: 'Entry was successfully created.' }
        format.json { render :show, status: :created, location: @entry }
        format.js
      else
        @entry_last = Entry.where(event: @entry.event, person: @entry.person).last
        format.html { render :new }
        format.json { render json: @entry.errors, status: :unprocessable_entity }
        #binding.pry
        format.js { render :error}
      end
    end
  end

  # PATCH/PUT /entries/1
  # PATCH/PUT /entries/1.json
  def update
    respond_to do |format|
      if @entry.update(entry_params)
        format.html { redirect_to @entry, notice: 'Entry was successfully updated.' }
        format.json { render :show, status: :ok, location: @entry }
      else
        helper_method :my_helper_method
        format.html { render :edit }
        format.json { render json: @entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /entries/1
  # DELETE /entries/1.json
  def destroy
    @entry.destroy
    respond_to do |format|
      format.html { redirect_to entries_url, notice: 'Entry was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def etiqueta
    render layout:false
  end

  def print
    respond_to :js
  end

  def certificado
    render layout: 'plenaria'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_entry
      @entry = Entry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def entry_params
      params.require(:entry).permit(
        :event_id, :person_id, :theme_id, :lunch, :tags, :flag,
        person_attributes:[:id, :cpf, :name, :phone, :email, :function, :city_id, :category, :birthdate,:sex,:company,:mothername]
        )
    end
  end
