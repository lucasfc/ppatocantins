class ReplyDeedsController < ApplicationController
  #before_action :authenticate_user!
  before_action :set_reply_deed, only: [:show, :edit, :update, :destroy, :delete_eq]
  skip_before_filter :verify_authenticity_token, :only => :destroy
  load_and_authorize_resource except: :create
  
  respond_to :html, :js

  def index
    @reply_deeds = ReplyDeed.all
    respond_with(@reply_deeds)
  end

  def show
    respond_with(@reply_deed)
  end

  def new
    @reply_deed = ReplyDeed.new
    respond_with(@reply_deed)
  end

  def edit
  end

  def create
    @reply_deed = ReplyDeed.new(reply_deed_params)
    if @reply_deed.save      
      index = 4 - params[:index].to_i
      replies = index.times{
        @reply_deed.dup.save
      }
      respond_with(@reply_deed)
    else
      render :error
    end
  end

  def update
    @reply_deed.update(reply_deed_params)
    respond_with(@reply_deed)
  end

  def destroy
    @reply_deed.destroy
    respond_to do |format|
      format.js
    end
  end

  def delete_eq
    ReplyDeed.where(deed: @reply_deed.deed, vote:@reply_deed.vote).delete_all
    render :destroy
  end

  private
    def set_reply_deed
      @reply_deed = ReplyDeed.find(params[:id])
    end

    def reply_deed_params
      params.require(:reply_deed).permit(
        :vote_id, :deed_id,
        deed_attributes:[:id, :name, :description, :subtheme_id, :code]
      )
    end
end
