class RepliesController < ApplicationController
  #before_action :authenticate_user!
  before_action :set_reply, only: [:show, :edit, :update, :destroy]
  before_action :get_questions, only: [:new, :edit, :update, :create]
  load_and_authorize_resource except: :create
  
  respond_to :html, :js, :json

  #Lista todos os registros
  def index
    @replies = Reply.all
    respond_with(@replies)
  end

  #Exibe um registro específico
  def show
    respond_with(@reply)
  end

  #Exibe o formulário de adição de registro
  def new
    @reply = Reply.new
    respond_with(@reply)
  end

  #Exibe o formulário de edição de registro  
  def edit
  end

  #Cria um registro com base em params
  def create
    @reply = Reply.new(reply_params)
    respond_to do |format|
      if @reply.save
        format.html { redirect_to @reply.question, notice: 'Resposta Arualizada com Sucesso' }
        format.json { render :show, status: :created, location: @reply }
        format.js
      else
        format.html { render :new }
        format.json { render json: @reply.errors, status: :unprocessable_entity }
        format.js { render :error}
      end
    end
  end

  #Atualiza um registro
  def update
    respond_to do |format|
      if @reply.update(reply_params)
        format.html { redirect_to @reply.question, notice: 'Resposta Arualizada com Sucesso' }
        format.json { render :show, status: :created, location: @reply }
        format.js
      else
        format.html { render :new }
        format.json { render json: @reply.errors, status: :unprocessable_entity }
        format.js { render :error}
      end
    end 
  end

  #Apaga um registro
  def destroy
    @reply.destroy
    respond_with(@reply)
  end

  private
    def set_reply
      @reply = Reply.find(params[:id])
    end

    def reply_params
      params.require(:reply).permit(:title, :description, :question_id, :default, :subtheme_id)
    end

    def get_questions
      @questions = Question.all.map { |q| [q.title, q.id]  }
      @subthemes = Subtheme.all.map { |q| [q.name, q.id]  }
    end
end
