class UsersController < ApplicationController
  #before_action :authenticate_user!
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource except: :create
  
  respond_to :html

  #Lista todos os registros
  def index
    @q = User.ransack(params[:q])
    @users = @q.result(distinct: true).page(params[:page]).per(10)
    respond_with(@users)
  end

  #Exibe um registro específico
  def show
    respond_with(@user)
  end

  #Exibe o formulário de adição de registro
  def new
    @user = User.new
    respond_with(@user)
  end

  #Exibe o formulário de edição de registro
  def edit
  end

  #Cria um registro com base em params
  def create
    @user = User.new(user_params)
    @user.save
    respond_with(@user)
  end

  #Atualiza um registro
  def update
    @user.update(user_params)
    respond_with(@user)
  end

  #Apaga um registro
  def destroy
    @user.destroy
    respond_with(@user)
  end

  def tokens
    @tokens = Token.where(used: [nil, false])
  end

  private
    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:cpf, :name, :email, :password, :password_confirmation)
    end
end
