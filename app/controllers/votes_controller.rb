class VotesController < ApplicationController
  require 'rest-client'

  #before_action :authenticate_user!, except: [:create, :receber_voto, :entrada_sala,:enviar_desafios]
  before_action :set_vote, only: [:show, :edit, :update, :destroy, :plus_vote]
  load_and_authorize_resource except: [:create, :receber_voto, :entrada_sala]
  # prepend_before_filter :require_no_authentication, :only => [:receber_voto]
  
  respond_to :html, :js

  #Lista todos os registros
  def index
    @votes = Vote.all
    respond_with(@votes)
  end

  def entrada_sala
    
    # @entry = Entry.where(tags: params["_json" ][0]["tags"].to_s, event_id: params["_json"][0]["event_id"].to_i).first()
    @person = Person.where(cpf: params["_json" ][0]["cpf"].to_s, event_id: params["_json"][0]["event_id"].to_i).first()

    @entry.flag = true

    if @entry.save      
      render :json => @entry
    else
      render :error
    end

  end

  def enviar_desafios
    RestClient.get 'http://ppacontrol.herokuapp.com', {params: Vote.all.to_json(tipo: 0)}
  end

  def receber_voto
    
    params["_json"].each do |t|
      # binding.pry
      @vote = Vote.find(t["vote_id"].to_i)
      @person = Person.where(cpf: t["cpf"]).first()

      # @entry = Entry.where(tags: params["_json" ][0]["tags"], event_id:@vote.event_id).first()
      # @entry = Entry.find(tags: params[:tags]).first()
      
      # @vote.votoplenaria = Vote.where("votoplenaria is not null").count
      #v = Vote.first.votoplenaria
      entry_vote = EntryVote.new(vote: @vote, entry: @person.entries.last)

      if entry_vote.save
        if (t["plenaria"].to_i) == 1

          begin
            @vote.votoplenaria += 1
          rescue
            @vote.votoplenaria = 1
          end

        else

          @vote.value += 1            

        end
        @vote.save
        # redirect_to @vote
        # respond_with(@vote)

      else
        # render :error
      end

    end
    redirect_to @vote


    end
  
  #Exibe um registro específico
  def show
    respond_with(@vote)
  end

  #Exibe o formulário de adição de registro 
  def new
    @vote = Vote.new
    @vote.reply = Reply.new
    @vote.event =  Event.find(params[:event_id])
    @vote.theme =  Theme.find(params[:theme_id])
    @subthemes =  @vote.theme.subthemes.map { |q| [q.name, q.id]  }
    respond_with(@vote)
  end

  #Exibe o formulário de edição de registro
  def edit
  end

  #Cria um registro com base em params
  def create
    @vote = Vote.new(vote_params)
    #binding.pry
    
    if @vote.save      
      respond_with(@vote)
    else
      render :error
    end
  end

  #Atualiza um registro
  def update
    @vote.update(vote_params)
    respond_with(@vote)
  end

  #Apaga um registro
  def destroy
    @vote.destroy
    respond_with(@vote)
  end

  #Cria um voto com as informações de Resposta Tema e Evento
  def create_by_reply
    @vote = Vote.new(
      reply: Reply.find(params[:reply_id]),
      theme: Theme.find(params[:theme_id]),
      event: Event.find(params[:event_id]),
      value: params[:value]
    )


    if !@vote.save
      render :error
    end

  end 

  def plus_vote
    if params[:remove] == 'true'
      @vote.remove_vote
    else
      @vote.plus_vote
    end
    render :create
  end

  private
    def set_vote
      @vote = Vote.find(params[:id])
    end

    def vote_params
      params.require(:vote).permit(
        :reply_id, :theme_id, :event_id, :value, :votoplenaria,
        reply_attributes:[
          :id, :title, :description, :question_id, :subtheme_id
       ] 
      )
    end
end
