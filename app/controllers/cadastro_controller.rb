class CadastroController < ApplicationController
	before_action :get_token
	skip_before_filter :authenticate_user!

	def cadastrar
		@user = User.new
	end

	def create
		@user = User.new(user_params)
		if @user.save
			@token.user = @user
			@token.used = true
			@token.save
		end
		sign_in_and_redirect @user, :event => :authentication
	end
	
	private

	def get_token
		@token = Token.find_by_token(params[:token])
		if @token.blank? or @token.used
			flash[:alert] = "Insira um token válido"
			render "get_token"	
		end		
	end

	def user_params
      params.require(:user).permit(:cpf, :name, :email, :password, :password_confirmation)
    end
end
