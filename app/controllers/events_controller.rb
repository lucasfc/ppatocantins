class EventsController < ApplicationController
  #before_action :authenticate_user!, except: [:create, :finalizar_votacao, :retornar_perguntas_plenarias, :retornar_desafios, :contabilizar_votos,:finalizar_votacao]
  before_action :set_event, only: [:show, :finalizar_votacao, :retornar_perguntas_plenarias, :edit, :update, :destroy , :names, :votacao_final, :definir_prioritario, :retornar_desafios]
  load_and_authorize_resource except: [:create, :finalizar_votacao, :retornar_perguntas_plenarias, :retornar_desafios]
  
  # GET /events
  # GET /events.json
  def index
    @events = Event.all
  end

  def contabilizar_votos
    votos = Vote.where(event_id: params[:event_id], theme_id: params[:theme_id])
    votos.each do |v|
      v.contabilizar_votos
    end
    redirect_to painel_moderador_path(event_id: params[:event_id], theme_id: params[:theme_id])
  end

  def contabilizar_votos_plenaria
    Event.find(params[:event_id]).contabilizar
    redirect_to painel_votacao_final_path(theme_id: Theme.last.id, event_id: params[:event_id])
  end 


  # GET /events/1
  # GET /events/1.json
  def show
  end

  # GET /events/new
  def new
    @event = Event.new
  end

  def finalizar_votacao
    @event.votes.update_all(prioritary: nil)
    voto = @event.votes.sort_by{|i| - i.votoplenaria}.first
    voto.update(prioritary: true)

    redirect_to painel_votacao_final_path(theme_id: voto.theme_id, event_id: voto.event_id)
  end

  def retornar_perguntas_plenarias

    perguntas = []

    @event.votes.order(:theme_id).group_by(&:theme).each do |theme,votes|

      voto = votes.sort_by{|i| - i.value}.first
      perguntas << {
        vote_id: "#{voto.id}p",
        desafio: voto.reply.title,
        event_id: voto.event_id,
        theme_id: "P",
        theme: "Plenária",
        event_name: voto.event.name,
        tipo: 1
      }
    end

    render json: {perguntas: perguntas}
    
  end

  def votacao_final

    # @votes = @event.votes.group_by(&:theme)
    # binding.pry
  end

  def definir_prioritario
    @event.votes.update_all(prioritary: nil)
    voto = Vote.find(params[:voto].to_i)
    voto.prioritary = true
    voto.save
    redirect_to painel_votacao_final_path(event_id: @event.id, theme_id: Theme.last.id)
  end

  # GET /events/1/edit
  def edit
  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new(event_params)

    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  #Show random name
  def names
    @person = @event.random_person
    respond_to do |format|
      format.js
    end
  end

  #retornar desafios
  def retornar_desafios
    render json: {perguntas: JSON.parse(@event.votes.where(theme_id: params[:theme_id]).to_json(tipo: 0))}
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:name, :place, :description, :date, :capacity, :capacity_room)
    end

end
