class DeedsController < ApplicationController
  #before_action :authenticate_user!
  before_action :set_deed, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource except: :create
  
  respond_to :html

  def index
    @deeds = Deed.all
    respond_with(@deeds)
  end

  def show
    respond_with(@deed)
  end

  def new
    @deed = Deed.new
    respond_with(@deed)
  end

  def edit
  end

  def create
    @deed = Deed.new(deed_params)
    @deed.save
    respond_with(@deed)
  end

  def update
    @deed.update(deed_params)
    respond_with(@deed)
  end

  def destroy
    @deed.destroy
    respond_with(@deed)
  end

  private
    def set_deed
      @deed = Deed.find(params[:id])
    end

    def deed_params
      params.require(:deed).permit(:name, :description, :subtheme_id, :code)
    end
end
