class CommentsController < ApplicationController
  #before_action :authenticate_user!
  before_action :set_comment, only: [:show, :edit, :update, :destroy, :editable_edit]
  skip_before_filter :verify_authenticity_token, :only => :editable_edit
  load_and_authorize_resource except: :create
  
  respond_to :html, :js
  
  #Lista todos os comentários
  def index
    @comments = Comment.all
    respond_with(@comments)
  end

  #Exibe apenas um comentário
  def show
    respond_with(@comment)
  end

  #Exibe o formulário de criação de um comentário
  def new
    @comment = Comment.new
    respond_with(@comment)
  end

  #Exibe o formulario de edição do comentário
  def edit
  end

  #Cria um comentário de acordo com os dados em comment_params
  def create
    @comment = Comment.new(comment_params)
    if @comment.save
      if params[:form_type] == "moderador"
        render :moderador_success
      end
    else
      render :error
    end
  end

  #Atualiza um registro através de comment_params
  def update
    @comment.update(comment_params)
    respond_with(@comment)
  end

  #Apaga um registro
  def destroy
    @comment.destroy
    redirect_to :back
  end


  #Permite a edição através do Jquery plugin X-editable
  def editable_edit
    @comment.text = params[:value]
    render json: @comment.save
  end

  private

    #Busca um comentário via params[:id]
    def set_comment
      @comment = Comment.find(params[:id])
    end

    #Configura os Strong Paramters
    def comment_params
      params.require(:comment).permit(:question_id, :theme_id, :event_id, :text, :tipo)
    end
end
