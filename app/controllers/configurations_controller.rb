class ConfigurationsController < ApplicationController
  #before_action :authenticate_user!
  before_action :set_configuration, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource except: :create
  
  respond_to :html

  #Lista todos os registros
  def index
    @configurations = Configuration.all
    respond_with(@configurations)
  end

  #Exibe um registro específico
  def show
    respond_with(@configuration)
  end

  #Exibe o formulário de adição de registro
  def new
    @configuration = Configuration.new
    respond_with(@configuration)
  end

  #Exibe o formulário de edição de registro
  def edit
  end

  #Cria um registro com base em params
  def create
    @configuration = Configuration.new(configuration_params)
    @configuration.save
    respond_with(@configuration)
  end

  #Atualiza um registro
  def update
    @configuration.update(configuration_params)
    respond_with(@configuration)
  end

  #Apaga um registro
  def destroy
    @configuration.destroy
    respond_with(@configuration)
  end

  private
    def set_configuration
      @configuration = Configuration.find(params[:id])
    end

    def configuration_params
      params.require(:configuration).permit(:token, :text)
    end
end
