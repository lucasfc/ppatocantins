class WebController < ApplicationController
	skip_before_filter :authenticate_user!
	before_action :get_inicial_params, only:[:presentation]

	def index
		@events = Event.all
	end

	def certificado
		if params[:cpf] and params[:event]
			person = Person.find_by_cpf(params[:cpf])
			event = Event.find(params[:event])
			entries = Entry.where(event: event, person:person)
			if entries.present?
				@entry = entries.last
				render 'entries/certificado'
			else
				flash[:notice] = "Certificado não encontrado"		
			end
		end
	end

	def presentation
		@deed_ranking = @event.deed_ranking(@theme)
		render "home/plenaria"
	end

	def document
		@event = Event.find(params[:id])
		@themes = Theme.capacity_dashboard(@event)
		@questions= Question.all
		respond_to do |format|
			format.html
			format.pdf do
				render pdf: "file_name",
				template: "web/document.pdf.erb",
				layout: "pdf.html"
			end
		end
	end

	private

	def get_inicial_params
		begin
			@event = Event.find(params[:event_id])
			@theme = Theme.find(params[:theme_id])
		rescue
			render "home/select_params"
		end
		@questions = Question.all.order(id: :asc)
	end
end
