class HomeController < ApplicationController
  before_action :authenticate_user!
  before_action :get_inicial_params, only:[
    :painel_moderador, :painel_redator, :lista_participantes, 
    :plenaria, :relatorio_evento,:painel_cidades, :painel_credenciamento,
    :painel_categorias, :telao, :painel_votacao_final
  ]
  before_action :get_credenciamento, only:[:painel_cidades, :painel_credenciamento,:painel_categorias]
  #load_and_authorize_resource :class => false
  
  #Lista todos os registros
  def index
  end

  #Exibe a página incial de credenciamento
  def credenciamento
  end

  #Exibe o painel do Moderador
  def painel_moderador
  end

  #Exibe a página para votação final
  def painel_votacao_final
    @event = Event.find(params[:event_id])
  end

  #Exibe o Plenária
  def plenaria
    @deed_ranking = @event.deed_ranking(@theme)
    render layout: 'plenaria'
  end

  #Exibe o Painel no Telão
  def telao
    @themes = Theme.all
    render layout: 'plenaria'
  end

  #Exibe o Relatório do Evento
  def relatorio_evento
    @deed_ranking = @event.deed_ranking_full
    @ranking_priority = @event.ranking_priority.take(10)
    respond_to do |format|
      format.docx{render filename:"#{@event}.docx", layout: false}
      format.html
    end
  end

  #Exibe o Painel do redator
  def painel_redator
    @comments_inicio =  @event.comments.inicio.where(theme:@theme)
    @comments_fim =  @event.comments.fim.where(theme:@theme)
    @deed_ranking = @event.deed_ranking(@theme)
    respond_to do |format|
      format.docx{render filename:"#{@theme} - #{@event}.docx", layout: false}
      format.html
    end
  end

  #Ainda não definido
  def painel
  end

  #Exibe o Painel de Credenciamento
  def painel_credenciamento
  end

  #Exibe o Painel com as informações das entradas por categoria
  def painel_categorias    
  end

  #Exibe os participantes cadastrados para o tema e evento
  def lista_participantes    
  end

  #Exibe um form de Entry no modal
  def credenciar
    @entry = Entry.new
    @person = Person.search_and_create(params[:cpf])
    @entry.event = Event.get_today
    @events = Event.all.map { |e| [e.name, e.id] }
    @themes = Theme.all.map { |e| [e.name, e.id] }
    @cities = City.all.map { |e| [e.name, e.id] }
    @entry.person = @person || Person.new
    respond_to do |format|
      format.js
    end
  end

  #Exibe as respostas cadastradas com default= true
  def get_actions
    @theme = Theme.find(params[:theme_id])
    @event = Event.find(params[:event_id])
    @deed_ranking = @event.deed_ranking(@theme)
    respond_to :js
  end

  #Exibe as respostas cadastradas como sociedade
  def get_sociedade
    @question = Question.find(params[:question_id])
    @theme = Theme.find(params[:theme_id])
    #binding.pry
    @q = Reply.ransack(params[:q])
    @replies = @q.result
    respond_to :js
  end

  def multimidia
    @entries = Entry.count
    @problems = Reply.count
    @actions= Deed.count
    respond_to do |format|
      format.html{render layout: 'plenaria'}
      format.js
    end
  end

  def multimidia_control
    render layout: 'plenaria'
  end

  private

    #Seta as informações iniciais dos paineis ou exibe um formulario para tanto
    def get_inicial_params
      begin
        @event = Event.find(params[:event_id])
        @theme = Theme.find(params[:theme_id])
      rescue
        render "select_params"
      end
      @questions = Question.all.order(id: :asc)
    end

    #Busca o evento do dia
    def get_credenciamento
      @themes = Theme.capacity_dashboard(@event)
      respond_to do |format|
        format.js
        format.html
      end
    end
end
