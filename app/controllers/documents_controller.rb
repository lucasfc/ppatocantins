class DocumentsController < ApplicationController
  #before_action :authenticate_user!
  before_action :set_document, only: [:show, :edit, :update, :destroy]
  before_action :get_themes, only: [:new, :edit, :update, :create]
  load_and_authorize_resource except: :create
  
  respond_to :html

  #Lista todos os registros
  def index
    @q = Document.ransack(params[:q])
    @documents = @q.result(distinct: true).page(params[:page]).per(10)
    respond_with(@documents)
  end

  #Exibe um registro específico
  def show
    respond_with(@document)
  end

  #Exibe o formulário de adição de registro
  def new
    @document = Document.new
    respond_with(@document)
  end

  #Exibe o formulário de edição de registro
  def edit
  end

  #Cria um registro com base em params
  def create
    @document = Document.new(document_params)
    @document.save
    respond_with(@document)
  end

  #Atualiza um registro
  def update
    @document.update(document_params)
    respond_with(@document)
  end

  #Apaga um registro
  def destroy
    @document.destroy
    respond_with(@document)
  end

  private
    def set_document
      @document = Document.find(params[:id])
    end

    def document_params
      params.require(:document).permit(:name, :description, :theme_id, :user_id, :file)
    end

    def get_themes
      @themes = Theme.all.map { |t| [t.name, t.id]  }
    end
end
