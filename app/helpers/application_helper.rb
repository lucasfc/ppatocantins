module ApplicationHelper
	def wicked_pdf_image_tag_for_public(img, options={})
		image_tag "file://#{Rails.root.join('public', 'images', img)}", options
	end
end
