module EventsHelper

	def bg_widget_capacity(foo)
		if foo
			"danger"
		else
			"success"
		end
	end
end
