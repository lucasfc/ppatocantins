class EventCity < ActiveRecord::Base
  belongs_to :city
  belongs_to :event
  has_paper_trail
end
