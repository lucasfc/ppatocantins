class Deed < ActiveRecord::Base
  belongs_to :subtheme

  	#Retorna a coluna <tt>name</tt> como padrão em string
	def to_s
		self.name.blank? ? self.description : self.name
	end
end
