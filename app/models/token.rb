class Token < ActiveRecord::Base
	belongs_to :user

	require 'uniquify'
	uniquify :token, :length => 6, :chars => 0..9
end
