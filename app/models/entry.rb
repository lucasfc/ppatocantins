class Entry < ActiveRecord::Base

  require 'rest-client'

  has_many :entry_vote
  belongs_to :event
  belongs_to :person
  belongs_to :theme
  has_paper_trail
  
  before_create :have_entry
  validates :theme, :event, :person, presence: true

  accepts_nested_attributes_for :person

  scope :lunch, ->{where(lunch:true)} 

  after_create :enviar_participante

  #Verifica se o participante se há duplicidade de entry  
  #do participante no evento para o mesmo tema
  def have_entry
  	if person and person.have_entry(event, theme)
  		errors.add(:theme,  " - Esta pessoa já foi cadastrada para este evento hoje")
  	end
  end


  def enviar_participante
    RestClient.post("#{ControlApi.provider}/consultas_publicas/receber_participante", {participante: JSON.parse(self.person.to_json)} )
  end
end
