class Question < ActiveRecord::Base
	has_many :replies
	has_many :votes, through: :replies
	has_paper_trail

	#Retorna a coluna <tt>title</tt> como padrão em string
	def to_s
		self.title
	end

	#Retorna um ranking com os votos para um determinado tema e evento,
	#A listagem é filtrada ao limite estabelecido na coluna <tt>limit</tt>
	#E ordenado decrescentemente pela coluna <tt>value</tt> em Vote.
	def votes_theme_ranking(theme, event)
		votes.where(theme:theme, event:event).where.not(value: 0).order(value: :desc).take(self.limit)
	end


	#Retorna todos os votos realizados
	def votes_theme(theme, event)
		votes.where(theme:theme).where(event:event)
	end

end
