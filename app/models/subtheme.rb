class Subtheme < ActiveRecord::Base
	has_paper_trail
	belongs_to :theme
	has_many :replies

	#Retorna a coluna <tt>name</tt> como padrão em string
	def to_s
		self.name
	end
end
