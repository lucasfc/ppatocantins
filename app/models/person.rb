class Person < ActiveRecord::Base
	has_many :entries
	has_many :events, through: :entries
	belongs_to :city
	has_paper_trail

	validates :cpf, uniqueness: true
	before_validation :cpf_default

	#extend Enumerize
	#enumerize :category, in: [:sociedade_civil_organizada, :poder_publico, :convidados, :outros], predicates: true
	#
	#
	#

	def sociedade_civil_organizada?
		self.category == "sociedade_civil_organizada"
	end

	def poder_publico?
		self.category == "poder_publico"
	end

	def convidados?
		self.category == "convidados"
	end

	def outros?
		self.category == "outros"
	end

	#Retorna a coluna <tt>name</tt> como padrão em string
	def to_s
		self.name
	end

	#Cadastra o CPF padrão como id do registro caso seja deixado em branco.
	#Isso se faz necessário pois o CPF é exigido como um campo único, mas
	#pode ser deixado em branco. Não sendo possível dois registros com CPF nil.
	def cpf_default
		if self.cpf.blank?			
			self.cpf = Time.now.to_i
		end
	end

	def self.use_api
		true
	end

	#Retorna Person pelo CPF caso exista, ou cria Person via CpfApi caso não exista.
	def self.search_and_create(cpf)
		person = find_by_cpf(cpf)
		if person.blank?
			begin
				if Person.use_api
					data = CpfApi.get_data(cpf)	
				else
					data = {status: 2}
				end
			end
			begin
				sex = data[:genero] == "M" ? "Masculino" : "Feminino"
			rescue
				sex = nil
			end
			if data and data[:status] == 1				
				return  Person.create(
					cpf:cpf,
					name: data[:nome],
					birthdate: data[:nascimento],
					mothername: data[:mae],
					sex: sex
				)
			end
		end
		return person
	end

	#Verifica se o existe um relacionamento entre Person e Theme para determinado Event via Entry.
	#Retorna true ou false
	def have_entry(event, theme)
		entries.where(event: event).where(theme: theme).present?
	end
	
	#Retorna os participantes da consulta pública
	def as_json(person = {})
		jason_person = (person)

		jason_person[:nome] = self.name
		jason_person[:cpf] = self.cpf
		jason_person[:email] = self.email
		jason_person[:categoria] = self.category

		#incluidos posteriormente
		jason_person[:cargo] = self.function
		jason_person[:empresa] = self.company
		jason_person[:sexo] = self.sex
		jason_person[:cidade] = self.city_id
		jason_person[:tag] = self.entries.select(:tags).where(:person_id == self.id).order(id: :asc).last.tags
		jason_person[:data] = self.birthdate
		jason_person[:mae] = self.mothername

		jason_person
	end

	
end
