class ControlApi

	#Configura a url do provedor que retorna os dados de CPF
	def self.provider
		"http://192.168.40.40:8080"
	end

	#Retorna os dados de acordo com o CPF informado quando houver conexão com a internet
	#Caso não haja conexão, ou o CPF for inválido, retorna vazio
	def self.get_votes(vote)			
		url = "#{provider}/colsultas_publicas/consultar_votos?vote_id=#{vote}"
		begin		
			client = RestClient.get(url)
			JSON.parse(client, symbolize_names: true)
		rescue
			{}
		end
	end

	def self.send_questions(votes)
		url = "#{provider}/colsultas_publicas/receber_perguntas"
		begin
			client = RestClient.post(url, {perguntas: vote})
			JSON.parse(client, symbolize_names: true)
		rescue
			{}
		end
	end


	def self.send_person(person)
		url = "#{provider}/colsultas_publicas/receber_participante"
		begin
			client = RestClient.post(url, {participante: person})
			JSON.parse(client, symbolize_names: true)
		rescue
			{}
		end
	end



	def self.totalizar_votacao(code)
		url = "#{provider}/consultas_publicas/totalizar_votacao?code=#{code}"
		begin
			client = RestClient.get(url)
			JSON.parse(client, symbolize_names: true)
		rescue
			{}
		end
	end
end
