class User < ActiveRecord::Base
	has_paper_trail
	# Include default devise modules. Others available are:
	# :confirmable, :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable,
	:recoverable, :rememberable, :trackable, :validatable

    #Retorna a coluna <tt>name</tt> como padrão em string
    def to_s
    	self.name
    end
end
