class Configuration < ActiveRecord::Base
	has_paper_trail
	validates :token, uniqueness: true
	validates :token, presence: true

	#Retorna o texto como string quando chamado o objeto
	def to_s
		self.text
	end

	#Busca uma configuração pelo token e retorna seu texto quando existir
	def self.get(token)
		Configuration.find_by_token(token) || "configuration::#{token}"
	end
end
