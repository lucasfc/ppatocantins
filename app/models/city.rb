class City < ActiveRecord::Base
	has_paper_trail

	#Retorna a coluna <tt>name</tt> como padrão em string
	#==== Exemplo:
	#puts City.last.to-s #=> 'Palmas'
	def to_s
		self.name
	end
end
