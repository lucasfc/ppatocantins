class Vote < ActiveRecord::Base
  require 'rest-client'
  has_many :entry_vote
  belongs_to :reply
  belongs_to :theme
  belongs_to :event
  has_many :reply_deeds, dependent: :destroy
  has_paper_trail

  accepts_nested_attributes_for :reply
  before_create :plenaria 

  validates :reply, uniqueness: { scope: [:event, :theme],
    message: "Esta resposta já foi inserida" }

  def contabilizar_votos
    response = JSON.parse(RestClient.get("#{ControlApi.provider}/consultas_publicas/consultar_votos?vote_id=#{self.id}", {}))
    self.update(value: response["conter"].to_i)
  end

  def contabilizar_votos_plenaria

    begin
      response = JSON.parse(RestClient.get("#{ControlApi.provider}/consultas_publicas/consultar_votos?vote_id=#{self.id}P", {}))  
      self.update(votoplenaria: response["conter"].to_i)
    rescue
    end

  end

  def plenaria
    self.votoplenaria = 0
  end

  #Insere mais um voto
  def plus_vote
  	value = self.value.to_i
  	self.value =  value + 1
  	save
  end

  #Remove um voto
  def remove_vote
  value = self.value.to_i
  self.value =  value - 1
  save
  end
   
  #Retorna os participantes da consulta pública
  def as_json(vote = {})
    lista_json = {}

    jason_votes = vote
    jason_votes[:desafio] = self.reply.title 
    jason_votes[:event_id] = self.event_id
    jason_votes[:theme_id] = self.theme_id
    jason_votes[:event_name] = self.event.name
    jason_votes[:theme] = self.theme.name
    jason_votes[:vote_id] = self.id


    jason_votes
    end

end
