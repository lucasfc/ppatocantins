class ReplyDeed < ActiveRecord::Base
  belongs_to :vote
  belongs_to :deed

  accepts_nested_attributes_for :deed

  validates :deed, presence: true
end
