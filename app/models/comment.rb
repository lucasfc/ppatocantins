class Comment < ActiveRecord::Base
  belongs_to :question
  belongs_to :theme
  belongs_to :event
  has_paper_trail

  #extend Enumerize
	#self.inheritance_column = nil
	#enumerize :tipo, in: [:tema, :inicio, :fim], predicates: true

	# #Retorna os comentários tipo tema
	scope :tema,-> {where(tipo: :tema)}

	# #Retorna os comentários tipo inicio
	scope :inicio,-> {where(tipo: :inicio)}

	# #Retorna os comentários tipo tema
	scope :fim,-> {where(tipo: :fim)}
	
	# #Ordena os comentários pelo id
	default_scope { order(id: :asc) }

  def tema?
    self.tipo == "tema"
  end

  def inicio?
    self.tipo == "tipo"
  end

  def fim?
    self.tipo == "fim"
  end
end
