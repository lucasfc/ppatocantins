class Document < ActiveRecord::Base
  belongs_to :theme
  belongs_to :user
  has_paper_trail

  mount_uploader :file, DocumentUploader

end
