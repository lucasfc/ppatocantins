class Reply < ActiveRecord::Base
  belongs_to :question
  belongs_to :subtheme
  has_many :votes

  scope :by_theme_question, lambda {|subtheme, question| where(subtheme: subtheme).where(question: question)}
  scope :norteadoras,-> {where(default: true)}
  scope :sociedade,-> {where(default: [false, nil])}

  #Retorna a coluna <tt>title</tt> como padrão em string
  def to_s
  	self.title
  end

end
