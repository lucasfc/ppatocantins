class EntryVote < ActiveRecord::Base
  belongs_to :entry
  belongs_to :vote
end
