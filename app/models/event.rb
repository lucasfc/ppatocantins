class Event < ActiveRecord::Base
	has_paper_trail
	has_many :entries
	has_many :people, through: :entries
	has_many :comments
	has_many :votes
	has_many :reply_deeds, through: :votes

	validates :capacity, :name, :place, :date, :capacity_room, presence: true

	#Ordena os registros do ultimo ao primeiro
	default_scope { order(id: :desc) }

	# Retorna o Evento do dia
	def self.get_today
		find_by_date(DateTime.now.to_date)
	end

	#Retorna todos os inscritos do evento
	def subscribers
		people.distinct
	end

	#Retorna o total de participantes inscritos no evento
	def total_subscribers
		subscribers.count
	end

	#Verifica se o total de inscritos ultrapassa a capacidade configurada
	def overbooked?
		total_subscribers > self.capacity
	end

	# Retorna o total da capacidade que foi atingida por inscritos.
	def percent_subscribers
		if overbooked?
			100
		else
			(total_subscribers * 100) / self.capacity
		end
	end

	#Retorna a quantidade de entradas que marcaram lunch como true
	def lunchs
		entries.lunch.count
	end

	#Retorna o array com um ranking de deeds por evento e tema
	def deed_ranking(theme)
		reply_deeds.includes(:vote).where(votes:{theme_id:theme.id}).group_by(&:deed).map do |deed, reply_deeds|
			{
				deed: deed,
				count: reply_deeds.count
			}
		end.sort{ |a,b| b[:count] <=> a[:count]}
	end

	#Retorna o array com um ranking de deeds por evento
	def deed_ranking_full
		reply_deeds.group_by(&:deed).map do |deed, reply_deeds|
			{
				deed: deed,
				count: reply_deeds.count
			}
		end.sort{ |a,b| b[:count] <=> a[:count]}
	end

	#Ranking de prioridades por evento
	def ranking_priority
		votes.group_by(&:reply).map do |reply, votes|
			{
				reply: reply,
				count: votes.sum{|v| v.value }
			}
		end.sort{ |a,b| b[:count] <=> a[:count]}
	end

	def can_edit
		self.date >= DateTime.now.to_date
	end

	def random_person
		people.offset(rand(people.count)).first
	end

	def contabilizar
		response = ControlApi.totalizar_votacao("E#{self.id}TP")
		response.map do |item|
			voto = Vote.find(item[:resposta][:code].delete("p"))
			voto.votoplenaria = item[:votos]
			voto.save
		end
		votes.order(votoplenaria: :desc).first.update(prioritary: true)
	end
end