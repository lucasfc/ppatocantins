class Theme < ActiveRecord::Base
	require 'rest-client'
	has_many :entries
	has_many :people, through: :entries
	has_many :subthemes
	has_many :votes
	has_paper_trail

	accepts_nested_attributes_for :subthemes, :allow_destroy => true
	default_scope { order(id: :asc) }
	#Retorna a coluna <tt>name</tt> como padrão em string
	def to_s
		self.name
	end

	

	#Retorna um array com as informações da capaciadade de cada tema em determinado evento
	def self.capacity_dashboard(event)
		Theme.all.map do |theme|
			{
				theme: theme,
				subscribers: entries = theme.total_subscribers(event),
				percent: (entries * 100) / event.capacity_room,
				overbooked: entries > event.capacity_room
			}
		end
	end

	#Retorna todos os Entry para este tema, filtrando por evento
	def subscribers(event)
		entries.where(event:event)
	end

	#Conta a quantidade de entry para este registro e evento.
	def total_subscribers(event)
		subscribers(event).count
	end

end
