json.array!(@replies) do |reply|
  json.extract! reply, :id, :title, :description, :question_id, :default, :subtheme_id
end
