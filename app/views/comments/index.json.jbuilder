json.array!(@comments) do |comment|
  json.extract! comment, :id, :question_id, :subtheme_id, :event_id, :text, :type
  json.url comment_url(comment, format: :json)
end
