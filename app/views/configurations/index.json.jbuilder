json.array!(@configurations) do |configuration|
  json.extract! configuration, :id, :token, :text
  json.url configuration_url(configuration, format: :json)
end
