json.array!(@people) do |person|
  json.extract! person, :id, :cpf, :name, :phone, :email, :function
  json.url person_url(person, format: :json)
end
