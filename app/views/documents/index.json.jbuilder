json.array!(@documents) do |document|
  json.extract! document, :id, :name, :description, :theme_id, :user_id
  json.url document_url(document, format: :json)
end
