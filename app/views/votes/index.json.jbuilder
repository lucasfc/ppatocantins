json.array!(@votes) do |vote|
  json.extract! vote, :id, :reply_id, :subtheme_id, :event_id, :value
  json.url vote_url(vote, format: :json)
end
