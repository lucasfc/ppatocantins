json.array!(@reply_deeds) do |reply_deed|
  json.extract! reply_deed, :id, :reply_id, :deed_id
  json.url reply_deed_url(reply_deed, format: :json)
end
