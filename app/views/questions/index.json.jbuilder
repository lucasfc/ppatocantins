json.array!(@questions) do |question|
  json.extract! question, :id, :title, :description, :limit
  json.url question_url(question, format: :json)
end
