json.array!(@deeds) do |deed|
  json.extract! deed, :id, :name, :description, :subtheme_id, :code
  json.url deed_url(deed, format: :json)
end
