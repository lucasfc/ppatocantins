json.array!(@entries) do |entry|
  json.extract! entry, :id, :event_id, :person_id, :theme_id
  json.url entry_url(entry, format: :json)
end
