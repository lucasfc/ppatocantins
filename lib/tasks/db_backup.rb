# encoding: utf-8

##
# Backup Generated: db_backup
# Once configured, you can run the backup with the following command:
#
# $ backup perform -t db_backup [-c <path_to_configuration_file>]
#
Backup::Model.new(:db_backup, 'Description for db_backup') do
  ##
  # Split [Splitter]
  #
  # Split the backup file in to chunks of 250 megabytes
  # if the backup file size exceeds 250 megabytes
  #
  split_into_chunks_of 250

  ##
  # PostgreSQL [Database]
  #
  database PostgreSQL do |db|
    db.name               = "ppa_development"
    db.username           = "postgres"
    db.password           = "postgres"
    db.host               = "localhost"
  end

  ##
  # FTP (File Transfer Protocol) [Storage]
  #
  store_with FTP do |server|
    server.username     = 'sistemas'
    server.password     = 'spl2015sistemas'
    server.ip           = "10.74.0.94" #'177.0.118.33'
    server.port         = 21
    server.path         = '~/backups/'
    server.keep         = 5
    server.passive_mode = false
  end

end
