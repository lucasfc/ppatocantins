require 'rails_helper'

RSpec.describe HomeController, type: :controller do

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #credenciamento" do
    it "returns http success" do
      get :credenciamento
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #painel_moderador" do
    it "returns http success" do
      get :painel_moderador
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #painel_redator" do
    it "returns http success" do
      get :painel_redator
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #painel" do
    it "returns http success" do
      get :painel
      expect(response).to have_http_status(:success)
    end
  end

end
