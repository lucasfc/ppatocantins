require 'rails_helper'

RSpec.describe WebController, type: :controller do

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #event" do
    it "returns http success" do
      get :event
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #presentation" do
    it "returns http success" do
      get :presentation
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #document" do
    it "returns http success" do
      get :document
      expect(response).to have_http_status(:success)
    end
  end

end
