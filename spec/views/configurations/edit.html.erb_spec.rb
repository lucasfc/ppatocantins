require 'rails_helper'

RSpec.describe "configurations/edit", type: :view do
  before(:each) do
    @configuration = assign(:configuration, Configuration.create!(
      :token => "MyString",
      :text => "MyText"
    ))
  end

  it "renders the edit configuration form" do
    render

    assert_select "form[action=?][method=?]", configuration_path(@configuration), "post" do

      assert_select "input#configuration_token[name=?]", "configuration[token]"

      assert_select "textarea#configuration_text[name=?]", "configuration[text]"
    end
  end
end
