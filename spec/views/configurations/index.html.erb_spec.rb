require 'rails_helper'

RSpec.describe "configurations/index", type: :view do
  before(:each) do
    assign(:configurations, [
      Configuration.create!(
        :token => "Token",
        :text => "MyText"
      ),
      Configuration.create!(
        :token => "Token",
        :text => "MyText"
      )
    ])
  end

  it "renders a list of configurations" do
    render
    assert_select "tr>td", :text => "Token".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
