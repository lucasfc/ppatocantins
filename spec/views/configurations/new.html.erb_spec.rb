require 'rails_helper'

RSpec.describe "configurations/new", type: :view do
  before(:each) do
    assign(:configuration, Configuration.new(
      :token => "MyString",
      :text => "MyText"
    ))
  end

  it "renders new configuration form" do
    render

    assert_select "form[action=?][method=?]", configurations_path, "post" do

      assert_select "input#configuration_token[name=?]", "configuration[token]"

      assert_select "textarea#configuration_text[name=?]", "configuration[text]"
    end
  end
end
