require 'rails_helper'

RSpec.describe "configurations/show", type: :view do
  before(:each) do
    @configuration = assign(:configuration, Configuration.create!(
      :token => "Token",
      :text => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Token/)
    expect(rendered).to match(/MyText/)
  end
end
