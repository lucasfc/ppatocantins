require 'rails_helper'

RSpec.describe "deeds/show", type: :view do
  before(:each) do
    @deed = assign(:deed, Deed.create!(
      :name => "Name",
      :description => "Description",
      :subtheme => nil,
      :code => "Code"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Description/)
    expect(rendered).to match(//)
    expect(rendered).to match(/Code/)
  end
end
