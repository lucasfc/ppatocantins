require 'rails_helper'

RSpec.describe "deeds/index", type: :view do
  before(:each) do
    assign(:deeds, [
      Deed.create!(
        :name => "Name",
        :description => "Description",
        :subtheme => nil,
        :code => "Code"
      ),
      Deed.create!(
        :name => "Name",
        :description => "Description",
        :subtheme => nil,
        :code => "Code"
      )
    ])
  end

  it "renders a list of deeds" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Code".to_s, :count => 2
  end
end
