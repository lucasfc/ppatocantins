require 'rails_helper'

RSpec.describe "deeds/new", type: :view do
  before(:each) do
    assign(:deed, Deed.new(
      :name => "MyString",
      :description => "MyString",
      :subtheme => nil,
      :code => "MyString"
    ))
  end

  it "renders new deed form" do
    render

    assert_select "form[action=?][method=?]", deeds_path, "post" do

      assert_select "input#deed_name[name=?]", "deed[name]"

      assert_select "input#deed_description[name=?]", "deed[description]"

      assert_select "input#deed_subtheme_id[name=?]", "deed[subtheme_id]"

      assert_select "input#deed_code[name=?]", "deed[code]"
    end
  end
end
