require 'rails_helper'

RSpec.describe "reply_deeds/new", type: :view do
  before(:each) do
    assign(:reply_deed, ReplyDeed.new(
      :reply => nil,
      :deed => nil
    ))
  end

  it "renders new reply_deed form" do
    render

    assert_select "form[action=?][method=?]", reply_deeds_path, "post" do

      assert_select "input#reply_deed_reply_id[name=?]", "reply_deed[reply_id]"

      assert_select "input#reply_deed_deed_id[name=?]", "reply_deed[deed_id]"
    end
  end
end
