require 'rails_helper'

RSpec.describe "reply_deeds/edit", type: :view do
  before(:each) do
    @reply_deed = assign(:reply_deed, ReplyDeed.create!(
      :reply => nil,
      :deed => nil
    ))
  end

  it "renders the edit reply_deed form" do
    render

    assert_select "form[action=?][method=?]", reply_deed_path(@reply_deed), "post" do

      assert_select "input#reply_deed_reply_id[name=?]", "reply_deed[reply_id]"

      assert_select "input#reply_deed_deed_id[name=?]", "reply_deed[deed_id]"
    end
  end
end
