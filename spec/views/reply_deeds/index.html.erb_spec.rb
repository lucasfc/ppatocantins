require 'rails_helper'

RSpec.describe "reply_deeds/index", type: :view do
  before(:each) do
    assign(:reply_deeds, [
      ReplyDeed.create!(
        :reply => nil,
        :deed => nil
      ),
      ReplyDeed.create!(
        :reply => nil,
        :deed => nil
      )
    ])
  end

  it "renders a list of reply_deeds" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
