require 'rails_helper'

RSpec.describe "documents/edit", type: :view do
  before(:each) do
    @document = assign(:document, Document.create!(
      :name => "MyString",
      :description => "MyText",
      :theme => nil,
      :user => nil
    ))
  end

  it "renders the edit document form" do
    render

    assert_select "form[action=?][method=?]", document_path(@document), "post" do

      assert_select "input#document_name[name=?]", "document[name]"

      assert_select "textarea#document_description[name=?]", "document[description]"

      assert_select "input#document_theme_id[name=?]", "document[theme_id]"

      assert_select "input#document_user_id[name=?]", "document[user_id]"
    end
  end
end
