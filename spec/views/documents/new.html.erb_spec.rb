require 'rails_helper'

RSpec.describe "documents/new", type: :view do
  before(:each) do
    assign(:document, Document.new(
      :name => "MyString",
      :description => "MyText",
      :theme => nil,
      :user => nil
    ))
  end

  it "renders new document form" do
    render

    assert_select "form[action=?][method=?]", documents_path, "post" do

      assert_select "input#document_name[name=?]", "document[name]"

      assert_select "textarea#document_description[name=?]", "document[description]"

      assert_select "input#document_theme_id[name=?]", "document[theme_id]"

      assert_select "input#document_user_id[name=?]", "document[user_id]"
    end
  end
end
