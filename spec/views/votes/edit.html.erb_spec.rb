require 'rails_helper'

RSpec.describe "votes/edit", type: :view do
  before(:each) do
    @vote = assign(:vote, Vote.create!(
      :reply => nil,
      :subtheme => nil,
      :event => nil,
      :value => 1
    ))
  end

  it "renders the edit vote form" do
    render

    assert_select "form[action=?][method=?]", vote_path(@vote), "post" do

      assert_select "input#vote_reply_id[name=?]", "vote[reply_id]"

      assert_select "input#vote_subtheme_id[name=?]", "vote[subtheme_id]"

      assert_select "input#vote_event_id[name=?]", "vote[event_id]"

      assert_select "input#vote_value[name=?]", "vote[value]"
    end
  end
end
