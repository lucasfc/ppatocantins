require 'rails_helper'

RSpec.describe "comments/new", type: :view do
  before(:each) do
    assign(:comment, Comment.new(
      :question => nil,
      :subtheme => nil,
      :event => nil,
      :text => "MyText",
      :type => ""
    ))
  end

  it "renders new comment form" do
    render

    assert_select "form[action=?][method=?]", comments_path, "post" do

      assert_select "input#comment_question_id[name=?]", "comment[question_id]"

      assert_select "input#comment_subtheme_id[name=?]", "comment[subtheme_id]"

      assert_select "input#comment_event_id[name=?]", "comment[event_id]"

      assert_select "textarea#comment_text[name=?]", "comment[text]"

      assert_select "input#comment_type[name=?]", "comment[type]"
    end
  end
end
