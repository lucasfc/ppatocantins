require 'rails_helper'

RSpec.describe "replies/edit", type: :view do
  before(:each) do
    @reply = assign(:reply, Reply.create!(
      :title => "MyString",
      :description => "MyText",
      :question => nil,
      :default => false,
      :subtheme => nil
    ))
  end

  it "renders the edit reply form" do
    render

    assert_select "form[action=?][method=?]", reply_path(@reply), "post" do

      assert_select "input#reply_title[name=?]", "reply[title]"

      assert_select "textarea#reply_description[name=?]", "reply[description]"

      assert_select "input#reply_question_id[name=?]", "reply[question_id]"

      assert_select "input#reply_default[name=?]", "reply[default]"

      assert_select "input#reply_subtheme_id[name=?]", "reply[subtheme_id]"
    end
  end
end
