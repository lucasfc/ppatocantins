require "rails_helper"

RSpec.describe ReplyDeedsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/reply_deeds").to route_to("reply_deeds#index")
    end

    it "routes to #new" do
      expect(:get => "/reply_deeds/new").to route_to("reply_deeds#new")
    end

    it "routes to #show" do
      expect(:get => "/reply_deeds/1").to route_to("reply_deeds#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/reply_deeds/1/edit").to route_to("reply_deeds#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/reply_deeds").to route_to("reply_deeds#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/reply_deeds/1").to route_to("reply_deeds#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/reply_deeds/1").to route_to("reply_deeds#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/reply_deeds/1").to route_to("reply_deeds#destroy", :id => "1")
    end

  end
end
